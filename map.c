/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 03:28:07 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/26 03:28:15 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_map		*new_map(void)
{
	t_map	*map;

	map = (t_map*)malloc(sizeof(t_map));
	if (!map)
		return (NULL);
	map->w = 0;
	map->h = 0;
	map->is_valid = 1;
	return (map);
}

void		delete_map(t_map *map)
{
	int		i;

	if (map->raw)
	{
		i = 0;
		while (map->raw[i])
		{
			delete_string(map->raw[i]);
			i++;
		}
		free(map->raw);
	}
	if (map->data)
	{
		i = 0;
		while (map->data[i])
		{
			free(map->data[i]);
			i++;
		}
		free(map->data);
	}
	free(map);
}
