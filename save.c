/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   save.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 11:32:55 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/24 11:32:56 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	int_split(unsigned char *start, int value)
{
	start[0] = (unsigned char)(value);
	start[1] = (unsigned char)(value >> 8);
	start[2] = (unsigned char)(value >> 16);
	start[3] = (unsigned char)(value >> 24);
}

int		bmfh_write(int fd, size_t size, t_window *win)
{
	int				i;
	int				tmp;
	unsigned char	bmfh[54];

	i = 0;
	while (i < 54)
		bmfh[i++] = 0;
	bmfh[0] = (unsigned char)('B');
	bmfh[1] = (unsigned char)('M');
	int_split(bmfh + 2, size);
	bmfh[10] = (unsigned char)(54);
	bmfh[14] = (unsigned char)(40);
	tmp = win->w;
	int_split(bmfh + 18, tmp);
	tmp = win->h;
	int_split(bmfh + 22, tmp);
	bmfh[27] = (unsigned char)(1);
	bmfh[28] = (unsigned char)(24);
	return (!(write(fd, bmfh, 54) < 0));
}

int		get_color(t_params *prm, int x, int y)
{
	int	rgb;
	int	color;

	color = *(int*)(prm->save->addr
			+ (4 * prm->window->w * (prm->window->h - 1 - y))
			+ (4 * x));
	rgb = (color & 0xFF0000) | (color & 0x00FF00) | (color & 0x0000FF);
	return (rgb);
}

int		bmp_write(int file, t_params *prm, int pad)
{
	const unsigned char	zero[3] = {0, 0, 0};
	int					i;
	int					j;
	int					color;

	i = 0;
	while (i < prm->window->h)
	{
		j = 0;
		while (j < prm->window->w)
		{
			color = get_color(prm, j, i);
			if (write(file, &color, 3) < 0)
				return (0);
			if (pad > 0 && write(file, &zero, pad) < 0)
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

int		save(t_params *prm)
{
	int			filesize;
	int			fd;
	int			pad;

	pad = (4 - (prm->window->w * 3) % 4) % 4;
	filesize = 54 + (3 * (prm->window->w + pad) * prm->window->h);
	if ((fd = open("screenshot.bmp", O_RDWR | O_CREAT
									| O_TRUNC | O_APPEND, 777)) < 0)
		return (0);
	if (!bmfh_write(fd, filesize, prm->window))
		return (0);
	if (!bmp_write(fd, prm, pad))
		return (0);
	close(fd);
	return (1);
}
