/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/21 21:38:05 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/21 21:38:07 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		expose_hook(void *params)
{
	t_vec		pos;
	size_t		sprites;
	t_params	*prm;
	double		distance;
	double		h;

	pos = vec_create(0, 0);
	prm = (t_params*)params;
	while (pos.x < prm->window->w)
	{
		pos.y = 0;
		distance = raycast(pos.x, prm);
		h = prm->level->wall->h / distance;
		column_draw(pos.x, h, prm->character->camera->ray, prm);
		pos.x++;
	}
	sprites = 0;
	while (sprites < prm->level->sprite->count)
	{
		sprite_draw(prm->level->sprite, sprites, prm->character, prm);
		sprites++;
	}
	return (0);
}

int		key_hook(int keycode, void *param)
{
	t_params	*prm;

	prm = (t_params*)param;
	if (keycode == ARROW_LEFT)
		look_left(prm->character);
	else if (keycode == ARROW_RIGHT)
		look_right(prm->character);
	else if (keycode == ARROW_UP || keycode == KEY_W)
		move_forward(prm->character);
	else if (keycode == ARROW_DOWN || keycode == KEY_S)
		move_backward(prm->character);
	else if (keycode == KEY_A)
		strafe_left(prm->character);
	else if (keycode == KEY_D)
		strafe_right(prm->character);
	else if (keycode == KEY_ESC)
		game_exit(prm, OK);
	expose_hook(prm);
	return (0);
}

int		exit_hook(t_params *params)
{
	game_exit(params, OK);
	return (0);
}
