/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sprite.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 03:33:56 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/26 03:34:09 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_vec		*new_pos_list(size_t count)
{
	t_vec	*pos_list;

	pos_list = (t_vec*)malloc(sizeof(t_vec) * (count + 1));
	if (!pos_list)
		return (NULL);
	return (pos_list);
}

t_sprite	*new_sprite(void)
{
	t_sprite	*sprite;

	sprite = (t_sprite*)malloc(sizeof(t_sprite));
	if (!sprite)
		return (NULL);
	sprite->h = 200;
	sprite->w = 200;
	sprite->pos_list = NULL;
	sprite->count = 0;
	sprite->texture = new_texture();
	return (sprite);
}

void		delete_sprite(t_sprite *sprite)
{
	if (sprite->texture)
		delete_texture(sprite->texture);
	if (sprite->pos_list)
		free(sprite->pos_list);
	free(sprite);
}
