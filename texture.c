/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 03:35:07 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/26 03:35:13 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_texture	*new_texture(void)
{
	t_texture	*tex;

	tex = (t_texture*)malloc(sizeof(t_texture));
	if (!tex)
		return (NULL);
	tex->w = 0;
	tex->h = 0;
	tex->bpp = 0;
	tex->len = 0;
	tex->endian = 0;
	return (tex);
}

void		delete_texture(t_texture *texture)
{
	if (texture->path)
		delete_string(texture->path);
	if (texture->ptr)
		mlx_destroy_image(g_mlx, texture->ptr);
	free(texture);
}
