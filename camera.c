/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   camera.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 03:24:20 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/26 03:24:30 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_camera	*new_camera(void)
{
	t_camera	*camera;

	camera = (t_camera*)malloc(sizeof(t_camera));
	if (!camera)
		return (NULL);
	camera->ray = new_ray();
	if (!camera->ray)
	{
		free(camera);
		return (NULL);
	}
	camera->zbuf = NULL;
	camera->rot = -1;
	camera->fov = 1.2;
	return (camera);
}

void		delete_camera(t_camera *camera)
{
	if (camera->zbuf)
		free(camera->zbuf);
	if (camera->ray)
		free(camera->ray);
	free(camera);
}
