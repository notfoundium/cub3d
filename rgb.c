/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rgb.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/18 02:51:57 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/18 02:53:16 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_color	rgb_set(unsigned int r, unsigned int g, unsigned int b)
{
	return (0 << 24 | r << 16 | g << 8 | b);
}

t_color	get_r(t_color rgb)
{
	return (rgb & (0xFF << 16));
}

t_color	get_g(t_color rgb)
{
	return (rgb & (0xFF << 8));
}

t_color	get_b(t_color rgb)
{
	return (rgb & 0xFF);
}
