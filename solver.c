/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solver.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 01:55:46 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/26 01:55:47 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		path_1(int x, int y, char **maze, char **bin)
{
	int	is_solved;

	is_solved = 0;
	if (is_moveable(maze[y - 1][x]) && bin[y - 1][x] == 0)
	{
		is_solved = maze_solver(x, y - 1, maze, bin);
		bin[y][x] = 0;
	}
	return (is_solved);
}

int		path_2(int x, int y, char **maze, char **bin)
{
	int	is_solved;

	is_solved = 0;
	if (is_moveable(maze[y][x + 1]) && bin[y][x + 1] == 0)
	{
		is_solved = maze_solver(x + 1, y, maze, bin);
		bin[y][x] = 0;
	}
	return (is_solved);
}

int		path_3(int x, int y, char **maze, char **bin)
{
	int	is_solved;

	is_solved = 0;
	if (is_moveable(maze[y + 1][x]) && bin[y + 1][x] == 0)
	{
		is_solved = maze_solver(x, y + 1, maze, bin);
		bin[y][x] = 0;
	}
	return (is_solved);
}

int		path_4(int x, int y, char **maze, char **bin)
{
	int	is_solved;

	is_solved = 0;
	if (is_moveable(maze[y][x - 1]) && bin[y][x - 1] == 0)
	{
		is_solved = maze_solver(x - 1, y, maze, bin);
		bin[y][x] = 0;
	}
	return (is_solved);
}

int		maze_solver(int x, int y, char **maze, char **bin)
{
	int		is_solved;

	is_solved = 0;
	if (maze[y][x] == EDGE)
		return (1);
	bin[y][x] = 1;
	if (path_1(x, y, maze, bin))
		return (1);
	if (path_2(x, y, maze, bin))
		return (1);
	if (path_3(x, y, maze, bin))
		return (1);
	if (path_4(x, y, maze, bin))
		return (1);
	return (is_solved);
}
