/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_path.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/21 22:44:27 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/21 22:44:30 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	parse_path(t_string *dir, t_string *line,
					t_flags *setflags, t_params *prm)
{
	t_string	*tmp;

	if (!string_equals_std(tmp = string_getword(line, 2), ""))
	{
		delete_string(tmp);
		delete_string(dir);
		delete_string(line);
		game_exit(prm, ERR);
	}
	delete_string(tmp);
	if (string_equals_std(dir, "S"))
		prm->level->sprite->texture->path = string_getword(line, 1);
	else if (string_equals_std(dir, "NO"))
		prm->level->wall->tex_n->path = string_getword(line, 1);
	else if (string_equals_std(dir, "SO"))
		prm->level->wall->tex_s->path = string_getword(line, 1);
	else if (string_equals_std(dir, "EA"))
		prm->level->wall->tex_e->path = string_getword(line, 1);
	else if (string_equals_std(dir, "WE"))
		prm->level->wall->tex_w->path = string_getword(line, 1);
	addflag(dir, line, setflags, prm);
}
