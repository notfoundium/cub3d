/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_res.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/21 22:44:11 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/21 22:44:13 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		checknbr(const char *str)
{
	int	i;
	int	res;

	i = 0;
	res = 0;
	while (str[i] != 0)
	{
		if (str[i] < '0' || str[i] > '9')
			return (-1);
		res *= 10;
		res += str[i] - '0';
		i++;
	}
	return (res);
}

void	parse_res(t_string *word, t_string *line,
					t_flags *flags, t_params *params)
{
	delete_string(word);
	word = string_getword(line, 3);
	if (!string_equals_std(word, ""))
		close_parser(word, line, params);
	word = string_getword(line, 1);
	params->window->w = checknbr(word->content);
	delete_string(word);
	word = string_getword(line, 2);
	params->window->h = checknbr(word->content);
	if (params->window->h <= 0 || params->window->w <= 0)
		close_parser(word, line, params);
	delete_string(word);
	*flags |= FLAG_R;
}
