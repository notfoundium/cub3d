# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gmegga <marvin@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/02/11 13:48:22 by gmegga            #+#    #+#              #
#    Updated: 2020/10/26 03:38:12 by gmegga           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = cub3D

# C compiler parameters

CC =		clang
CFLAGS =	-Wall -Wextra -Werror
CSTD =		-std=c99

# Sources

SRC_DIR = .
SOURCE	=	main.c \
			camera.c \
			character.c \
			graphics.c \
			hooks.c \
			init.c \
			is_valid.c \
			level.c \
			map_parser.c \
			map.c \
			movement.c \
			params.c \
			parse_color.c \
			parse_path.c \
			parse_res.c \
			parser_core.c \
			parser.c \
			ray.c \
			raycast.c \
			rgb.c \
			rotation.c \
			save.c \
			solver.c \
			sprite_writer.c \
			sprite.c \
			texture.c \
			utils.c \
			wall_writer.c \
			wall.c \
			window.c

SRC_PATH	=	$(addprefix $(SRC_DIR)/, $(SRC_FILES))

OBJECTS =	main.o \
			camera.o \
			character.o \
			graphics.o \
			hooks.o \
			init.o \
			is_valid.o \
			level.o \
			map_parser.o \
			map.o \
			movement.o \
			params.o \
			parse_color.o \
			parse_path.o \
			parse_res.o \
			parser_core.o \
			parser.o \
			ray.o \
			raycast.o \
			rgb.o \
			rotation.o \
			save.o \
			solver.o \
			sprite_writer.o \
			sprite.o \
			texture.o \
			utils.o \
			wall_writer.o \
			wall.o \
			window.o

LIBS = -lXext -lX11 -lm

# Make rules

.PHONY: lib all clean fclean re norme so

# libft compile

lib:
	@make -C libft/ all

# Cub3D compile

%.o: %.c
	$(CC) $(CSTD) $(CFLAGS) $< -c

all: $(NAME)

$(NAME): lib $(OBJECTS)
	@$(CC) $(CFLAGS) $(CSTD) $(OBJECTS) ./libft/libft.a ./mlx/libmlx.a $(LIBS) -o $(NAME)
	@echo "Cub3D compiled!"

clean:
	@rm -f $(OBJECTS)
	@rm -f *.gch
	@rm -f $(NAME)
	@make -C libft/ clean
	@echo "All objects removed"

fclean:	clean
	@rm -f $(NAME)
	@rm -f *.so
	@make -C libft/ fclean
	@echo "All files removed"

re:
	@$(MAKE) fclean
	@$(MAKE) all

debug: lib
	@$(CC) $(CFLAGS) $(CSTD) $(SOURCE) ./libft/libft.a ./mlx/libmlx.a $(LIBS) -o $(NAME) -g -O0 -D DEBUG
	@echo "Cub3D compiled!"

norme:
	@norminette $(SOURCE) $(INCLUDES)
