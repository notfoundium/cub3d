/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_valid.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 11:33:16 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/24 11:33:17 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		is_valid(t_params *params)
{
	if (params->character->is_init != 1 ||
		params->level->map->is_valid != 1 ||
		params->character->pos.x == -1 ||
		params->character->pos.y == -1 ||
		params->window->h < 120 || params->window->h > 4800 ||
		params->window->w < 160 || params->window->w > 7680 ||
		!params->level->sprite->texture ||
		!params->level->wall->tex_e ||
		!params->level->wall->tex_n ||
		!params->level->wall->tex_s ||
		!params->level->wall->tex_w)
		return (0);
	return (1);
}

int		is_valid_source(const char *filename, const char *pattern)
{
	size_t	len;

	if (!filename)
		return (0);
	if (!is_valid_file(filename))
		return (0);
	len = ft_strlen(filename);
	if (len < 4)
		return (0);
	if (ft_strcmp(filename + len - 4, pattern))
		return (0);
	return (1);
}

int		is_valid_file(const char *path)
{
	int		fd;

	fd = open(path, O_RDONLY);
	if (fd < 0)
		return (0);
	close(fd);
	return (1);
}

int		is_start(char c)
{
	return (c == 'S' || c == 'N' || c == 'E' || c == 'W');
}

int		is_moveable(char c)
{
	if (c == WALL)
		return (0);
	else
		return (1);
}
