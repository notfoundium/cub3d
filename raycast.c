/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycast.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/21 21:18:40 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/21 21:18:42 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_vec	calculate_vector(t_camera *camera, t_window *win, double column)
{
	t_vec	result;
	t_vec	vec1;
	t_vec	vec2;

	vec1 = vec_create(cos(camera->rot), sin(camera->rot));
	vec2 = vec_create(sin(camera->rot), -cos(camera->rot));
	vec1 = vec_mult(vec1, (double)win->w / (2.0 * tan(camera->fov / 2.0)));
	vec2 = vec_mult(vec2, (double)column - win->w / 2.0);
	result = vec_sum(vec1, vec2);
	result = vec_norm(result);
	return (result);
}

void	set_side(t_vec pos, t_vec prev_pos, t_ray *ray)
{
	if ((int)pos.x != prev_pos.x)
	{
		if (prev_pos.x - pos.x > 0)
		{
			ray->side = SIDE_E;
			ray->offset = pos.y - floor(pos.y);
		}
		else
		{
			ray->side = SIDE_W;
			ray->offset = ceil(pos.y) - pos.y;
		}
		return ;
	}
	if (prev_pos.y - pos.y > 0)
	{
		ray->side = SIDE_S;
		ray->offset = ceil(pos.x) - pos.x;
	}
	else
	{
		ray->side = SIDE_N;
		ray->offset = pos.x - floor(pos.x);
	}
}

int		in_bound(t_vec pos, t_params *prm)
{
	int	bound_x;
	int	bound_y;

	bound_x = prm->level->map->w;
	bound_y = prm->level->map->h;
	if (pos.x > 0 && pos.x < bound_x &&
		pos.y > 0 && pos.y < bound_y)
		return (1);
	else
		return (0);
}

double	raycast(int column, t_params *prm)
{
	t_vec		pos;
	t_vec		prev_pos;
	t_vec		result;
	t_camera	*camera;
	double		distance;

	camera = prm->character->camera;
	pos = prm->character->pos;
	prev_pos = prm->character->pos;
	result = calculate_vector(prm->character->camera, prm->window, column);
	while (in_bound(pos, prm) &&
			prm->level->map->data[(int)pos.y][(int)pos.x] != WALL)
	{
		pos.x = pos.x + result.x * camera->ray->delta;
		pos.y = pos.y + result.y * camera->ray->delta;
		distance = sqrt(pow(pos.x - prm->character->pos.x, 2) +
						pow(pos.y - prm->character->pos.y, 2));
		set_side(pos, prev_pos, camera->ray);
		prev_pos.x = (int)pos.x;
		prev_pos.y = (int)pos.y;
	}
	camera->zbuf[column] = distance;
	return (distance *
			vec_dot(result, vec_create(cos(camera->rot), sin(camera->rot))));
}
