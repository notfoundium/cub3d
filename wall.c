/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wall.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/21 20:34:03 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/21 20:34:06 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_wall		*new_wall(void)
{
	t_wall	*wall;

	wall = (t_wall*)malloc(sizeof(t_wall));
	if (!wall)
		return (NULL);
	wall->tex_n = new_texture();
	wall->tex_s = new_texture();
	wall->tex_e = new_texture();
	wall->tex_w = new_texture();
	wall->h = 500;
	return (wall);
}

void		delete_wall(t_wall *wall)
{
	if (wall->tex_e)
		delete_texture(wall->tex_e);
	if (wall->tex_n)
		delete_texture(wall->tex_n);
	if (wall->tex_s)
		delete_texture(wall->tex_s);
	if (wall->tex_w)
		delete_texture(wall->tex_w);
	free(wall);
}
