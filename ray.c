/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 03:34:51 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/26 03:34:57 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_ray		*new_ray(void)
{
	t_ray	*ray;

	ray = (t_ray*)malloc(sizeof(t_ray));
	if (!ray)
		return (NULL);
	ray->delta = 0.01;
	ray->side = -1;
	return (ray);
}

void		delete_ray(t_ray *ray)
{
	free(ray);
}
