/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   window.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 03:33:02 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/26 03:33:10 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_window	*new_window(void)
{
	t_window	*window;

	window = (t_window*)malloc(sizeof(t_window));
	if (!window)
		return (NULL);
	window->ptr = NULL;
	window->h = -1;
	window->w = -1;
	return (window);
}

void		delete_window(t_window *window)
{
	if (window->ptr)
		mlx_destroy_window(g_mlx, window->ptr);
	free(window);
}

void		new_screen(t_params *prm)
{
	t_texture	*screen;

	screen = (t_texture*)malloc(sizeof(t_texture));
	screen->ptr = mlx_new_image(g_mlx, prm->window->w, prm->window->h);
	screen->addr = (unsigned char*)mlx_get_data_addr(screen->ptr,
						&screen->bpp, &screen->len, &screen->endian);
	prm->save = screen;
}
