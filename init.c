/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 03:30:14 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/26 03:30:16 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		tex_init(t_texture *tex)
{
	if (!tex)
		return (-1);
	if (!tex->path)
		return (-1);
	if (!tex->path->content)
		return (-1);
	if (!is_valid_source(tex->path->content, ".xpm"))
		return (-1);
	if (tex->path->content[0] == '.')
		tex->ptr = mlx_xpm_file_to_image(g_mlx,
									&tex->path->content[2], &tex->w, &tex->h);
	else if (tex->path->content[0] == '/')
		tex->ptr = mlx_xpm_file_to_image(g_mlx,
									tex->path->content, &tex->w, &tex->h);
	if (!tex->ptr)
		return (-1);
	tex->addr = (unsigned char*)mlx_get_data_addr(tex->ptr,
										&tex->bpp, &tex->len, &tex->endian);
	return (OK);
}

void	set_winsize(t_window *win)
{
	int	screen_w;
	int	screen_h;

	mlx_get_screen_size(g_mlx, &screen_w, &screen_h);
	win->w = (win->w < screen_w) ? (win->w) : (screen_w);
	win->h = (win->h < screen_h) ? (win->h) : (screen_h);
	win->ptr = mlx_new_window(g_mlx, win->w, win->h, "Cub3D");
}

int		init(t_params *params, int mode)
{
	t_window	*win;
	t_level		*lvl;
	t_character	*character;
	int			check_sum;

	check_sum = 0;
	win = params->window;
	lvl = params->level;
	character = params->character;
	lvl->wall->h = (win->w / 2.0) / tan(params->character->camera->fov / 2.0);
	character->camera->zbuf = (double*)malloc(win->w * sizeof(double));
	if (!character->camera->zbuf)
		return (-1);
	check_sum += tex_init(lvl->wall->tex_n);
	check_sum += tex_init(lvl->wall->tex_s);
	check_sum += tex_init(lvl->wall->tex_e);
	check_sum += tex_init(lvl->wall->tex_w);
	check_sum += tex_init(lvl->sprite->texture);
	if (check_sum < 0)
		return (ERR);
	if (mode != 1)
		set_winsize(win);
	return (OK);
}

char	**new_charr(size_t w, size_t h)
{
	size_t	i;
	char	**arr;

	i = 0;
	arr = (char**)malloc(sizeof(char*) * (h + 1));
	if (!arr)
		return (NULL);
	while (i < h)
	{
		arr[i] = (char*)malloc(sizeof(char) * (w + 1));
		ft_bzero(arr[i], w + 1);
		i++;
	}
	arr[i] = NULL;
	return (arr);
}

void	delete_charr(char **arr)
{
	size_t	i;

	i = 0;
	while (arr[i] != NULL)
		free(arr[i++]);
	free(arr);
}
