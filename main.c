/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/21 09:54:44 by gmegga            #+#    #+#             */
/*   Updated: 2020/09/21 14:14:44 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	game_exit(t_params *params, int code)
{
	delete_params(params);
	if (code == 0)
		exit(0);
	ft_println(ERR_MSG);
	exit(-1);
}

void	play(const char *mapname, int mode)
{
	t_params	*params;

	params = new_params(mode);
	load_config(mapname, params);
	if (init(params, mode) == ERR)
		game_exit(params, ERR);
	if (!is_valid(params))
		game_exit(params, ERR);
	if (mode == 1)
	{
		new_screen(params);
		expose_hook(params);
		save(params);
		game_exit(params, OK);
	}
	mlx_key_hook(params->window->ptr, key_hook, params);
	mlx_hook(params->window->ptr, 17, 1 << 17, &exit_hook, params);
	mlx_expose_hook(params->window->ptr, expose_hook, params);
	mlx_loop(g_mlx);
}

int		main(int argc, char **argv)
{
	if (argc == 2)
		play(argv[1], 0);
	else if (argc == 3 && !ft_strcmp(argv[2], "--save"))
		play(argv[1], 1);
	else
	{
		ft_println(ERR_MSG);
		return (-1);
	}
	return (0);
}
