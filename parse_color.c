/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_color.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/21 22:44:19 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/21 22:44:20 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		color_check(int red, int green, int blue)
{
	if (red > 256 || red < 0 ||
		green > 256 || green < 0 ||
		blue > 256 || blue < 0)
		return (-1);
	return (0);
}

int		read_color(char *str, int *color)
{
	int		i;

	i = 0;
	if (*str < '0' || *str > '9')
		return (-1);
	*color = 0;
	while (str[i] != ',' && str[i] != 0)
	{
		if (str[i] < '0' || str[i] > '9')
			return (-1);
		*color *= 10;
		*color += str[i++] - '0';
	}
	return (i);
}

int		parse_rgb(char *str, int *red, int *green, int *blue)
{
	int		i;
	int		current;

	i = 0;
	if ((current = read_color(str, red)) < 0)
		return (-1);
	i += current + 1;
	if ((current = read_color(str + i, green)) < 0)
		return (-1);
	i += current + 1;
	if (str[i] < '0' || str[i] > '9')
		return (-1);
	*blue = 0;
	while (!ft_isspace(str[i]) && str[i] != 0)
	{
		if (str[i] < '0' || str[i] > '9')
			return (-1);
		*blue *= 10;
		*blue += str[i++] - '0';
	}
	return (color_check(*red, *green, *blue));
}

void	parse_color(t_string *word, t_string *line,
					t_flags *flags, t_params *prm)
{
	int		red;
	int		green;
	int		blue;
	t_color	*dest;

	red = -1;
	green = -1;
	blue = -1;
	dest = string_equals_std(word, "F") ? (&prm->level->floor) :
											(&prm->level->ceilling);
	addflag(word, line, flags, prm);
	word = string_getword(line, 2);
	if (!string_equals_std(word, ""))
		close_parser(word, line, prm);
	delete_string(word);
	word = string_getword(line, 1);
	if (parse_rgb(word->content, &red, &green, &blue) < 0)
		close_parser(word, line, prm);
	*dest = rgb_set(red, green, blue);
	delete_string(word);
}
