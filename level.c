/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   level.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 03:25:12 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/26 03:25:14 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_level		*new_level(void)
{
	t_level	*level;

	if (!(level = (t_level*)malloc(sizeof(t_level))))
		return (NULL);
	if (!(level->wall = new_wall()))
	{
		free(level);
		return (NULL);
	}
	if (!(level->sprite = new_sprite()))
	{
		delete_wall(level->wall);
		free(level);
		return (NULL);
	}
	if (!(level->map = new_map()))
	{
		delete_wall(level->wall);
		delete_sprite(level->sprite);
		free(level);
		return (NULL);
	}
	level->ceilling = 0;
	level->floor = 0;
	return (level);
}

void		delete_level(t_level *level)
{
	if (level->map)
		delete_map(level->map);
	if (level->wall)
		delete_wall(level->wall);
	if (level->sprite)
		delete_sprite(level->sprite);
	free(level);
}
