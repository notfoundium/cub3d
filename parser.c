/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 03:27:40 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/26 03:27:50 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_parser	*new_parser(void)
{
	t_parser	*parser;

	if (!(parser = (t_parser*)malloc(sizeof(t_parser))))
		return (NULL);
	parser->file = NULL;
	parser->bin = NULL;
	parser->data = NULL;
	return (parser);
}

void		delete_parser(t_parser *parser)
{
	if (parser->bin)
		delete_charr(parser->bin);
	if (parser->file)
		delete_file(parser->file);
	free(parser);
}
