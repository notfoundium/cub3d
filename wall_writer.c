/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wall_writer.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 03:37:51 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/26 03:37:52 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_texture	*tex_get(int side, t_wall *wall)
{
	if (side == SIDE_N)
		return (wall->tex_n);
	else if (side == SIDE_S)
		return (wall->tex_s);
	else if (side == SIDE_E)
		return (wall->tex_e);
	else if (side == SIDE_W)
		return (wall->tex_w);
	return (NULL);
}

void		draw_env(t_pixel p, t_params *prm)
{
	p.color = (p.pos.y > prm->window->h / 2) ?
	(prm->level->floor) : (prm->level->ceilling);
	pixel_put(p, prm);
}

void		draw_wall(int h, t_pixel p, t_ray *ray, t_params *prm)
{
	int			y_up;
	t_texture	*tex;

	y_up = prm->window->h / 2 - h / 2;
	tex = tex_get(ray->side, prm->level->wall);
	p.color = get_pixel(tex,
						(int)floor(ray->offset * tex->w),
						(int)floor(((p.pos.y - y_up) / h) * tex->h));
	pixel_put(p, prm);
}

void		column_draw(int col_index, double h, t_ray *ray, t_params *prm)
{
	t_window	*win;
	t_camera	*camera;
	t_pixel		p;

	camera = prm->character->camera;
	win = prm->window;
	p.pos = vec_create(col_index, 0);
	while (p.pos.y < prm->window->h)
	{
		if (p.pos.y > (prm->window->h / 2 - h / 2) &&
			p.pos.y < (prm->window->h / 2 + h / 2))
			draw_wall(h, p, ray, prm);
		else
			draw_env(p, prm);
		p.pos.y++;
	}
}
