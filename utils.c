/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 02:58:04 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/26 02:58:05 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_string	**sarr_resize(t_string **old, size_t *size)
{
	t_string	**new;
	size_t		i;

	i = 0;
	*size = *size * 2;
	new = (t_string**)malloc(*size * sizeof(t_string*));
	while (i < *size)
		new[i++] = NULL;
	i = 0;
	while (old[i])
	{
		new[i] = old[i];
		i++;
	}
	free(old);
	return (new);
}

int			find_c(char c, const char *str)
{
	int		i;
	size_t	count;

	i = 0;
	count = 0;
	while (str[i] != 0)
	{
		if (str[i] == c)
			count++;
		i++;
	}
	return (count);
}

void		skip_lines(t_file *file, t_string **line, t_string **word)
{
	*word = string_getword(*line, 0);
	while (string_equals_std(*word, ""))
	{
		delete_string(*word);
		delete_string(*line);
		*line = file_readline(file);
		if (!*line)
			break ;
		*word = string_getword(*line, 0);
	}
}

size_t		push_char(char *std, char c)
{
	size_t	i;

	i = 0;
	while (std[i] != 0)
		i++;
	std[i] = c;
	return (i);
}

int			is_find_err(const char *std, char a, char b)
{
	int	i;

	i = 0;
	while (std[i] != 0)
	{
		if (std[i] != a && std[i] != b)
			return (1);
		i++;
	}
	return (0);
}
