/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   movement.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/18 02:42:13 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/18 02:42:16 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	move_forward(t_character *character)
{
	character->pos.x += character->move_speed * cos(character->camera->rot);
	character->pos.y += character->move_speed * sin(character->camera->rot);
}

void	move_backward(t_character *character)
{
	character->pos.x -= character->move_speed * cos(character->camera->rot);
	character->pos.y -= character->move_speed * sin(character->camera->rot);
}

void	strafe_left(t_character *character)
{
	character->pos.x -= character->move_speed * sin(character->camera->rot);
	character->pos.y += character->move_speed * cos(character->camera->rot);
}

void	strafe_right(t_character *character)
{
	character->pos.x += character->move_speed * sin(character->camera->rot);
	character->pos.y -= character->move_speed * cos(character->camera->rot);
}
