/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sprite_writer.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 03:31:11 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/26 03:31:12 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

double	simplify_angle(double ang)
{
	double	r;

	r = fmod(ang, 2 * PI);
	if (fabs(r) > PI)
		r += 2 * PI * ((r < 0) ? (1) : (-1));
	return (r);
}

double	calculate_column(int current, t_util *util, t_params *prm)
{
	t_vec		v;
	double		rel_ang;
	t_character *character;
	t_sprite	*sprite;

	sprite = prm->level->sprite;
	character = prm->character;
	v = vec_sum(sprite->pos_list[current], vec_mult(character->pos, -1));
	rel_ang = character->camera->rot - atan2(v.y, v.x);
	rel_ang = simplify_angle(rel_ang);
	if (fabs(rel_ang) > character->camera->fov / 2)
		return (-1.0);
	util->d = vec_len(v);
	util->p = vec_dot(v,
					vec_create(cos(character->camera->rot),
					sin(character->camera->rot)));
	return ((int)(prm->window->w / 2.0 *
			(1.0 + tan(rel_ang) /
			tan(character->camera->fov / 2))));
}

void	sprite_put(t_pixel pix, t_vec pos, t_util *util, t_params *prm)
{
	t_sprite	*sprite;

	sprite = prm->level->sprite;
	pix.color = get_pixel(sprite->texture,
					(double)pos.x / (sprite->w / util->p) * sprite->texture->w,
					(double)pos.y / (sprite->w / util->p) * sprite->texture->h);
	if (pix.color != 0)
	{
		pix.pos.y = prm->window->h / 2 +
					prm->level->wall->h / util->p / 2 -
					sprite->h / util->p + pos.y;
		pixel_put(pix, prm);
	}
}

void	sprite_draw(t_sprite *sprite, int curr, t_character *c, t_params *prm)
{
	int		column;
	int		i;
	int		j;
	t_pixel	pix;
	t_util	util;

	column = calculate_column(curr, &util, prm);
	if (column < 0 || column > prm->window->w)
		return ;
	i = 0;
	while (i++ < sprite->w / util.p)
	{
		pix.pos.x = i + column - sprite->w / util.p / 2;
		if (abs((int)pix.pos.x) > prm->window->w || pix.pos.x < 0)
			return ;
		j = pix.pos.x;
		if (c->camera->zbuf[j] > util.d)
		{
			c->camera->zbuf[j] = util.d;
			j = 0;
			while (j++ < sprite->h / util.p)
				sprite_put(pix, vec_create(i, j), &util, prm);
		}
	}
}
