/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   params.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 03:28:29 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/26 03:28:31 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_params	*new_params(int mode)
{
	t_params	*parameters;

	g_mlx = mlx_init();
	if (!(parameters = (t_params*)malloc(sizeof(t_params))))
		return (NULL);
	if (!(parameters->level = new_level()))
	{
		free(parameters);
		return (NULL);
	}
	if (!(parameters->character = new_character()))
	{
		delete_level(parameters->level);
		free(parameters);
		return (NULL);
	}
	if (!(parameters->window = new_window()))
	{
		delete_character(parameters->character);
		delete_level(parameters->level);
		free(parameters);
		return (NULL);
	}
	parameters->mode = mode;
	return (parameters);
}

void		delete_params(t_params *parameters)
{
	if (parameters->level)
		delete_level(parameters->level);
	if (parameters->window)
		delete_window(parameters->window);
	if (parameters->character)
		delete_character(parameters->character);
	if (parameters->mode == 1)
	{
		mlx_destroy_image(g_mlx, parameters->save->ptr);
		free(parameters->save);
	}
	free(parameters);
}
