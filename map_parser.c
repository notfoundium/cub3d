/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_parser.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 03:34:33 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/26 03:34:40 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	char_case(t_string **raw, t_vec *pos, t_params *prm, size_t *count)
{
	char	**data;
	size_t	curr;

	data = prm->level->map->data;
	if ((raw[(int)pos->y - 1]->content[(int)pos->x]) == ' ')
		push_char(data[(int)pos->y], EDGE);
	else if ((raw[(int)pos->y - 1]->content[(int)pos->x]) == EMPTY)
		push_char(data[(int)pos->y], EMPTY);
	else if ((raw[(int)pos->y - 1]->content[(int)pos->x]) == WALL)
		push_char(data[(int)pos->y], WALL);
	else if ((raw[(int)pos->y - 1]->content[(int)pos->x]) == SPRITE)
	{
		curr = push_char(data[(int)pos->y], EMPTY);
		prm->level->sprite->pos_list[(*count)++] = vec_create(curr, pos->y);
	}
	else if (is_start(raw[(int)pos->y - 1]->content[(int)pos->x]) &&
			!prm->character->is_init)
	{
		curr = push_char(data[(int)pos->y], EMPTY);
		character_setstart(prm->character,
		vec_create(curr, pos->y), raw[(int)pos->y - 1]->content[(int)pos->x]);
	}
	else
		prm->level->map->is_valid = 0;
	pos->x++;
}

void	convert_map(t_string **raw, t_params *prm)
{
	char		**data;
	t_vec		pos;
	t_sprite	*sprite;
	size_t		count;

	pos.y = 0;
	count = 0;
	sprite = prm->level->sprite;
	prm->level->map->w += 2;
	prm->level->map->h += 2;
	prm->level->map->data = new_charr(prm->level->map->w, prm->level->map->h);
	data = prm->level->map->data;
	ft_memset(data[(int)pos.y++], EDGE, prm->level->map->w);
	while (pos.y < prm->level->map->h - 1)
	{
		pos.x = 0;
		push_char(data[(int)pos.y], EDGE);
		while ((raw[(int)pos.y - 1]->content[(int)pos.x]) != 0)
			char_case(raw, &pos, prm, &count);
		push_char(data[(int)pos.y++], EDGE);
	}
	ft_memset(data[(int)pos.y], EDGE, prm->level->map->w);
}

void	finish_parse_map(t_params *prm, t_string *word,
							t_string *line, size_t h)
{
	t_sprite	*sprite;
	t_map		*map;

	map = prm->level->map;
	sprite = prm->level->sprite;
	prm->level->map->h = h;
	if (map->h < 3 || map->w < 3)
		close_parser(word, line, prm);
	if (!(sprite->pos_list = new_pos_list(sprite->count)))
		close_parser(word, line, prm);
	convert_map(map->raw, prm);
	prm->parser->bin = new_charr(map->w, map->h);
	if (maze_solver(prm->character->pos.x, prm->character->pos.y,
							map->data, prm->parser->bin))
		game_exit(prm, ERR);
}

void	parse_map(t_string *line, t_parser *parser, t_params *prm)
{
	t_string	*word;
	size_t		i;
	size_t		size;

	i = 0;
	size = 1;
	skip_lines(parser->file, &line, &word);
	(!line) ? (game_exit(prm, ERR)) : (0);
	if (is_find_err(word->content, ' ', '1'))
		close_parser(word, line, prm);
	if (!(prm->level->map->raw = (t_string**)malloc(sizeof(t_string*))))
		close_parser(word, line, prm);
	while (line && !string_equals_std(word, ""))
	{
		delete_string(word);
		(i + 1 >= size) ?
		(prm->level->map->raw = sarr_resize(prm->level->map->raw, &size)) : (0);
		(line->len > prm->level->map->w) ?
						(prm->level->map->w = line->len) : (0);
		prm->level->sprite->count += find_c('2', line->content);
		prm->level->map->raw[i++] = line;
		line = file_readline(parser->file);
		word = string_getword(line, 0);
	}
	finish_parse_map(prm, word, line, i);
}
