/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_core.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 03:33:42 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/26 03:33:44 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void		close_parser(t_string *word, t_string *line, t_params *prm)
{
	if (prm->parser)
		delete_parser(prm->parser);
	if (word)
		delete_string(word);
	if (line)
		delete_string(line);
	game_exit(prm, ERR);
}

void		addflag(t_string *word, t_string *line,
						t_flags *flags, t_params *prm)
{
	if (string_equals_std(word, "S") && (*flags & FLAG_S) == 0)
		*flags |= FLAG_S;
	else if (string_equals_std(word, "NO") && (*flags & FLAG_NO) == 0)
		*flags |= FLAG_NO;
	else if (string_equals_std(word, "SO") && (*flags & FLAG_SO) == 0)
		*flags |= FLAG_SO;
	else if (string_equals_std(word, "EA") && (*flags & FLAG_EA) == 0)
		*flags |= FLAG_EA;
	else if (string_equals_std(word, "WE") && (*flags & FLAG_WE) == 0)
		*flags |= FLAG_WE;
	else if (string_equals_std(word, "F") && (*flags & FLAG_F) == 0)
		*flags |= FLAG_F;
	else if (string_equals_std(word, "C") && (*flags & FLAG_C) == 0)
		*flags |= FLAG_C;
	else
		close_parser(word, line, prm);
	delete_string(word);
}

t_string	*check_lines(t_file *file, t_string *line,
						t_params *params, unsigned int *flags)
{
	t_string	*word;

	while ((line = file_readline(file)) && *flags < 255)
	{
		word = string_getword(line, 0);
		if (string_equals_std(word, ""))
		{
			delete_string(word);
			continue;
		}
		else if (string_equals_std(word, "R"))
			parse_res(word, line, flags, params);
		else if (string_equals_std(word, "F") ||
				string_equals_std(word, "C"))
			parse_color(word, line, flags, params);
		else if (string_equals_std(word, "NO") ||
				string_equals_std(word, "SO") ||
				string_equals_std(word, "WE") ||
				string_equals_std(word, "EA") ||
				string_equals_std(word, "S"))
			parse_path(word, line, flags, params);
		delete_string(line);
	}
	return (line);
}

int			is_empty_end(int fd)
{
	int	c;

	if (fd < 0)
		return (1);
	c = ft_getchar_fd(fd);
	while (c >= 0)
	{
		if (c != ' ' && c != '\n')
			return (0);
		c = ft_getchar_fd(fd);
	}
	return (1);
}

void		load_config(const char *filename, t_params *prm)
{
	unsigned int	flags;
	t_string		*current_str;

	current_str = NULL;
	prm->parser = new_parser();
	if (!is_valid_source(filename, ".cub"))
		game_exit(prm, ERR);
	if (!(prm->parser->file = new_file(filename, O_RDONLY)))
		game_exit(prm, ERR);
	flags = 0;
	current_str = check_lines(prm->parser->file, current_str, prm, &flags);
	if (flags == 255 && current_str)
	{
		parse_map(current_str, prm->parser, prm);
		if (!is_empty_end(prm->parser->file->fd))
		{
			delete_parser(prm->parser);
			game_exit(prm, ERR);
		}
		delete_parser(prm->parser);
	}
	else
		delete_parser(prm->parser);
}
