/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub3d.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/21 14:16:11 by gmegga            #+#    #+#             */
/*   Updated: 2020/09/21 14:22:18 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUB3D_H
# define CUB3D_H
# include <math.h>
# include "libft/libft.h"
# include "mlx/mlx.h"

# ifndef PI
#  define PI 3.14159265358979323846
# endif

# ifndef INT_MIN
#  define INT_MIN -2147483648
# endif

# ifndef INT_MAX
#  define INT_MAX 2147483647
# endif

# ifndef UINT_MAX
#  define UINT_MAX 4294967295
# endif

# ifndef ERR_MSG
#  define ERR_MSG "Error"
# endif

void			*g_mlx;

enum			e_objects
{
	EMPTY = '0',
	WALL = '1',
	SPRITE = '2',
	EDGE = '#'
};

enum			e_sides
{
	SIDE_N = 0,
	SIDE_E = 1,
	SIDE_S = 2,
	SIDE_W = 3
};

enum			e_exitcodes
{
	OK = 0,
	ERR = -1
};

enum			e_setflags
{
	FLAG_R = 1 << 0,
	FLAG_NO = 1 << 1,
	FLAG_SO = 1 << 2,
	FLAG_WE = 1 << 3,
	FLAG_EA = 1 << 4,
	FLAG_S = 1 << 5,
	FLAG_F = 1 << 6,
	FLAG_C = 1 << 7
};

enum			e_keys
{
	ARROW_LEFT = 65361,
	ARROW_UP = 65362,
	ARROW_RIGHT = 65363,
	ARROW_DOWN = 65364,
	KEY_ESC = 65307,
	KEY_W = 119,
	KEY_S = 115,
	KEY_A = 97,
	KEY_D = 100
};

typedef struct	s_util
{
	double	p;
	double	d;
}				t_util;

typedef	struct	s_pixel
{
	t_vec	pos;
	t_color	color;
}				t_pixel;

typedef struct	s_ray
{
	double		offset;
	double		delta;
	int			side;
}				t_ray;

typedef struct	s_texture
{
	t_string		*path;
	void			*ptr;
	int				w;
	int				h;
	unsigned char	*addr;
	int				bpp;
	int				len;
	int				endian;
}				t_texture;

typedef	struct	s_camera
{
	double		fov;
	double		rot;
	double		*zbuf;
	t_ray		*ray;
}				t_camera;

typedef	struct	s_character
{
	int			is_init;
	double		move_speed;
	double		rot_speed;
	t_vec		pos;
	t_camera	*camera;
}				t_character;

typedef	struct	s_window
{
	void		*ptr;
	int			h;
	int			w;
}				t_window;

typedef	struct	s_wall
{
	double		h;
	t_texture	*tex_n;
	t_texture	*tex_s;
	t_texture	*tex_w;
	t_texture	*tex_e;
}				t_wall;

typedef	struct	s_sprite
{
	t_texture	*texture;
	t_vec		*pos_list;
	size_t		count;
	double		h;
	double		w;
}				t_sprite;

typedef struct	s_map
{
	t_string	**raw;
	size_t		w;
	size_t		h;
	char		**data;
	int			is_valid;
}				t_map;

typedef	struct	s_level
{
	t_color		ceilling;
	t_color		floor;
	t_map		*map;
	t_wall		*wall;
	t_sprite	*sprite;
}				t_level;

typedef struct	s_parser
{
	t_file		*file;
	char		**data;
	char		**bin;
}				t_parser;

typedef	struct	s_params
{
	t_window	*window;
	t_character	*character;
	t_level		*level;
	t_texture	*save;
	t_parser	*parser;
	int			mode;
}				t_params;

/*
********************************************************************************
**                              CONSTRUCTORS                                  **
********************************************************************************
*/

void			new_screen(t_params *prm);
char			**new_charr(size_t w, size_t h);
t_vec			*new_pos_list(size_t count);
t_ray			*new_ray();
t_window		*new_window();
t_camera		*new_camera();
t_character		*new_character();
t_texture		*new_texture();
t_sprite		*new_sprite();
t_map			*new_map();
t_wall			*new_wall();
t_level			*new_level();
t_params		*new_params(int mode);
t_parser		*new_parser();

/*
********************************************************************************
**                               DESTRUCTORS                                  **
********************************************************************************
*/

void			delete_charr(char **arr);
void			delete_ray(t_ray *ray);
void			delete_window(t_window *window);
void			delete_camera(t_camera *camera);
void			delete_character(t_character *character);
void			delete_texture(t_texture *texture);
void			delete_sprite(t_sprite *sprite);
void			delete_map(t_map *map);
void			delete_wall(t_wall *wall);
void			delete_level(t_level *level);
void			delete_params(t_params *params);
void			delete_parser(t_parser *parser);

/*
********************************************************************************
**                                UTILITIES                                   **
********************************************************************************
*/

size_t			push_char(char *std, char c);
int				maze_solver(int x, int y, char **maze, char **bin);
int				find_c(char c, const char *str);
int				is_start(char c);
int				is_valid(t_params *params);
int				is_valid_file(const char *path);
int				is_valid_source(const char *filename, const char *pattern);
t_string		**sarr_resize(t_string **old, size_t *size);
void			skip_lines(t_file *file, t_string **line, t_string **word);
void			character_setstart(t_character *character, t_vec pos, char dir);
int				is_find_err(const char *std, char a, char b);
int				is_moveable(char c);
double			simplify_angle(double ang);

/*
********************************************************************************
**                                 PARSER                                     **
********************************************************************************
*/

void			addflag(t_string *word, t_string *line,
							t_flags *flags, t_params *prm);
void			close_parser(t_string *word, t_string *line, t_params *prm);
void			load_config(const char *filename, t_params *params);
void			int_split(unsigned char *start, int value);
void			parse_res(t_string *word, t_string *line,
					t_flags *flags, t_params *params);
void			parse_path(t_string *dir, t_string *line,
					t_flags *setflags, t_params *prm);
void			parse_color(t_string *word, t_string *line,
					t_flags *flags, t_params *prm);
void			parse_map(t_string *line, t_parser *parser, t_params *prm);

/*
********************************************************************************
**                                MOVEMENT                                    **
********************************************************************************
*/

void			move_forward(t_character *character);
void			move_backward(t_character *character);
void			look_left(t_character *character);
void			look_right(t_character *character);
void			strafe_right(t_character *character);
void			strafe_left(t_character *character);

/*
********************************************************************************
**                               Colors (RGB)                                 **
********************************************************************************
*/

unsigned int	rgb_set(unsigned int r, unsigned int g, unsigned int b);
unsigned int	get_r(unsigned int rgb);
unsigned int	get_g(unsigned int rgb);
unsigned int	get_b(unsigned int rgb);

/*
********************************************************************************
**                                  GRAPHICS                                  **
********************************************************************************
*/

int				get_pixel(t_texture *tex, int x, int y);
void			pixel_put(t_pixel pixel, t_params *prm);
double			raycast(int column, t_params *prm);
void			column_draw(int col_index, double h, t_ray *ray, t_params *prm);
void			sprite_draw(t_sprite *sprite, int current,
							t_character *character, t_params *prm);
int				key_hook(int keycode, void *param);
int				expose_hook(void *params);
int				exit_hook(t_params *params);

/*
********************************************************************************
**                                    GAME                                    **
********************************************************************************
*/
int				init(t_params *params, int mode);
void			game_exit(t_params *params, int code);
int				save(t_params *prm);

#endif
