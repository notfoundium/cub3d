/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   character.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 02:53:19 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/26 02:53:20 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_character	*new_character(void)
{
	t_character *character;

	character = (t_character*)malloc(sizeof(t_character));
	if (!character)
		return (NULL);
	character->camera = new_camera();
	if (!character->camera)
	{
		free(character);
		return (NULL);
	}
	character->is_init = 0;
	character->pos = vec_create(3.0, 3.0);
	character->move_speed = 0.3;
	character->rot_speed = PI / 8;
	return (character);
}

void		delete_character(t_character *character)
{
	if (character->camera)
		delete_camera(character->camera);
	free(character);
}

void		character_setstart(t_character *character, t_vec pos, char dir)
{
	character->is_init++;
	character->pos = pos;
	if (dir == 'S')
		character->camera->rot = -PI / 2;
	else if (dir == 'N')
		character->camera->rot = PI / 2;
	else if (dir == 'E')
		character->camera->rot = PI;
	else if (dir == 'W')
		character->camera->rot = 0;
}
