/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotation.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/18 02:41:10 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/18 02:41:31 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	look_left(t_character *character)
{
	if (character->camera->rot - character->rot_speed < 2 * PI)
		character->camera->rot += character->rot_speed;
	else
		character->camera->rot += character->rot_speed - 2 * PI;
}

void	look_right(t_character *character)
{
	if (character->camera->rot - character->rot_speed > 0)
		character->camera->rot -= character->rot_speed;
	else
		character->camera->rot -= character->rot_speed + 2 * PI;
}
