/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   graphics.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 11:30:12 by gmegga            #+#    #+#             */
/*   Updated: 2020/10/24 11:30:14 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		get_pixel(t_texture *tex, int x, int y)
{
	return (*(int*)(tex->addr + (4 * tex->w * y) + (4 * (int)x)));
}

void	pixel_put(t_pixel pixel, t_params *prm)
{
	int			x;
	int			y;
	t_window	*win;

	x = pixel.pos.x;
	y = pixel.pos.y;
	win = prm->window;
	if (prm->mode == 0)
		mlx_pixel_put(g_mlx, win->ptr, x, y, pixel.color);
	else
		int_split(&prm->save->addr[4 * y * win->w + 4 * x], pixel.color);
}
